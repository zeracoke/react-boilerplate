import React from "react";
import { Link } from "react-router-dom";

// header component for display routing
function Header() {
	return (
		<div className="top">
			<ul>
				<li>
					<Link to="/">Home</Link>
				</li>
			</ul>
		</div>
	);
}

export default Header;
