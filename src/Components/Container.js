import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";

import Main from "./Main"
import List from "./List"
import Warning from "./Warning"

function Container({ location }) {
	return (
		//container with routing
		<Switch location={location}>
			<Route exact path="/" component={Main} />
			<Route path="/list" component={List} />
			<Route path="/warning" component={Warning} />
		</Switch>
	);
}

export default withRouter(Container);
