import React from "react";

class Warning extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			error: ""
		};
	}

	render() {
		return (
			<div className="wrap container-fluid">
				<div className="row warning">
					<h1>Something bad happened</h1>
					<p>{this.props.location.state.error}</p>
				</div>
			</div>
		);
	}
}

export default Warning;
