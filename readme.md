# React parcel boilerplate

## Installation

**`npm install` Installs node modules.**

**`npm run dev` Starts parcel boundler with hot reload at a free port. [http://localhost:1234](http://localhost:1234)**

### Full command list

```bash
"format": "prettier --write \"src/**/*.{js,jsx,css,scss,json} \"",
"format:check": "prettier --list-different \"src/**/*.{js,jsx,css,scss,json} \"",
"lint": "eslint \"src/*.{js,jsx}\"",
"lint:fix": "eslint --fix \"src/*.{js,jsx}\"",
"dev": "parcel src/index.html"
"build": "parcel build src/index.html"
```

### Build

`npm run build`

### Dev environment

- Windows 10 64bit
- NodeJS v10.11.0
- NPM v6.4.1

### Used libs

For versions please see *package.json* file.

- React
- React-DOM
- React-Router-DOM
- Axios
- [Flexbox grid](http://flexboxgrid.com/)

### Tools

- Babel
- Eslint
- Prettier
- Autoprefixer
- EsLint
- Parcel

## License

[ISC](https://choosealicense.com/licenses/isc/)
